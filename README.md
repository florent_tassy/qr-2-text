![Mozilla Add-on rating](https://img.shields.io/amo/stars/qr-2-text?style=flat-square)
![Mozilla Add-on version](https://img.shields.io/amo/v/qr-2-text?style=flat-square)
![License](https://img.shields.io/gitlab/license/37614927?style=flat-square)
![Mozilla Add-on users](https://img.shields.io/amo/users/qr-2-text?style=flat-square)


<img src="public/icon-dark.svg" alt="QR 2 Text icon" height="150">

# QR 2 Text 
QR 2 Text is a non-intrusive Firefox add-on that can read a pasted image to retrieve and decode a QR code. It uses [jsQR](https://github.com/cozmo/jsQR) library for QR code decoding, [Svelte](https://svelte.dev/) as a front-end framework and [Vite](https://vitejs.dev/) as a build tool.  

[![Get the add-on !](https://extensionworkshop.com/assets/img/documentation/publish/get-the-addon-178x60px.dad84b42.png)](https://addons.mozilla.org/firefox/addon/qr-2-text/)

## How does it work ?
Once the add-on is installed, a new icon appears in Firefox toolbar :  

![Firefox toolbar with QR 2 Text button](images/QR2Text_browser_action.png)

To decode a QR code :
- Copy an image containing a QR code
- Click on the icon
- Paste the image

<img src="images/QR2Text_example.gif" alt="QR 2 Text example">

## Why a QR code scanner on my computer ? I have a phone that makes the job 🤔
That's a fair point. But regularly, it happens that I need to scan a QR code on my computer screen : it can be a QR code displayed as part of a web conference, a [Swiss QR-bill](https://www.kmu.admin.ch/kmu/en/home/concrete-know-how/finances/accounting-and-auditing/introduction-of-the-qr-bill.html), a [COVID digital certificate](https://en.wikipedia.org/wiki/EU_Digital_COVID_Certificate) (... well, okay, the use case isn't obvious here), a [two-steps authentication setup](https://blog.mozilla.org/services/2018/05/22/two-step-authentication-in-firefox-accounts/), or simply a QR-code found over the internet.

In that case, here are the choices :
- scan from a phone and send the result to the computer. That's slow and not handy, I need the phone, and it sounds silly to scan my computer screen to send the result to this same computer.
- use a desktop application. But frankly, there are not that many, at least on GNU/Linux, they require many permissions that I am reluctant to grant for just decoding a QR code (network access ??), are sometimes heavy (my previous one was a 34 MB Flatpak) and buggy...
- use an existing Firefox add-on. That's a lighter solution, but again, according to their permissions, they could be quite intrusive... And they often read QR codes through webcam, which is usually not what I need.
- code my own solution, simpler, working standalone and without any permission, and covering only the use case I mentioned above : scanning from images. So here comes QR 2 Text 🙂 Less than 76 Kb i.e. 400 times smaller than my previous app, doing what I need without spreading anything over the internet or activating my webcam !  

### What permissions are needed ?
None, QR 2 Text is as non-intrusive as possible.

### Does QR 2 Text collect my data ?
No, and it will never do.

## Build from source

Building the add-on from source requires to have a working [Node.js](https://nodejs.org). In addition, some package.json scripts would work only in a "*nix" environment. The add-on can be built by running the following command from its root directory:  
```
npm ci  
npm run build:addon
```

## Legal notice
Firefox is a registered trademark of the Mozilla Foundation.  
Node.js is a trademark of the OpenJS Foundation.  
QR Code is registered trademarks of DENSO WAVE INCORPORATED.  

The above-mentioned trademarks are only used to refer to products.  
QR 2 Text and its developer are not affiliated, sponsored nor endorsed by any of the above-mentioned organizations.  

## Acknowledgments
QR 2 Text uses Font Awesome icons available under [Creative Commons Attribution 4.0 International license](https://fontawesome.com/license).

## Changelog
1.1.0 -> update dependencies, improve README, add LICENSE, rationalize icons, add tests
1.0.0 -> first release  
