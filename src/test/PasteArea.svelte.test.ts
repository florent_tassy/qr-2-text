import "@testing-library/jest-dom";
import { render } from "@testing-library/svelte";
import PasteArea from "../lib/PasteArea.svelte";

describe("PasteArea", () => {
    test("shows the paste area when rendered", () => {
        const { getByRole } = render(PasteArea);
        expect(getByRole("region")).toBeInTheDocument();
        expect(getByRole("region")).toBeVisible();
    });

    test("image is hidden when rendered", () => {
        const { getByTestId } = render(PasteArea);
        expect(getByTestId("img")).toBeInTheDocument();
        expect(getByTestId("img")).not.toBeVisible();
    });

    test("canvas is hidden when rendered", () => {
        const { getByTestId } = render(PasteArea);
        expect(getByTestId("canvas")).toBeInTheDocument();
        expect(getByTestId("canvas")).not.toBeVisible();
    });
});
