import "@testing-library/jest-dom";
import { render, screen } from '@testing-library/svelte';
import App from "../App.svelte";

describe("App", () => {
    test("shows the addon title when rendered", () => {
        render(App);
        expect(screen.getByText("QR 2 Text")).toBeInTheDocument();
        expect(screen.getByText("QR 2 Text")).toBeVisible();
    });

    test("shows the addon instruction when rendered", () => {
        render(App);
        expect(screen.getByTitle("instruction")).toBeInTheDocument();
        expect(screen.getByTitle("instruction")).toBeVisible();
    });

    test("shows the paste area when rendered", () => {
        render(App);
        expect(screen.getByRole("region")).toBeInTheDocument();
        expect(screen.getByRole("region")).toBeVisible();
    });
});
