import Decoder from "src/lib/Decoder";
import { JSDOM } from "jsdom";
import { expectTypeOf } from "vitest";

describe("Decoder with empty image", () => {
    const dom = new JSDOM(`<!DOCTYPE html>
    <html>
        <body>
            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABHNCSVQICAgIfAhkiAAAAJlJREFUaEPtktEJgDAQxayDlO6/pIIjBAJypP8vcEnX3ue5Brx7wA3fCR3yt5IVqYhkoK8licXYimB10rAikliMrQhWJw0rIonF2IpgddKwIpJYjK0IVicNKyKJxdiKYHXSsCKSWIytCFYnDSsiicXYimB10rAikliMrQhWJw0rIonF2IpgddKwIpJYjK0IVicNKyKJxdgxRV4HeQG/ggiKcwAAAABJRU5ErkJggg==" />
            <canvas />
        </body>
    </html>`, { resources: "usable" });

    const canvas = dom.window.document.body.getElementsByTagName("canvas")[0];
    const img = dom.window.document.body.getElementsByTagName("img")[0];

    const decoder: Decoder = new Decoder(img, canvas);

    expectTypeOf(decoder.getDecoded).toBeFunction()

    let decoded: string = "";

    img.onload = () => {
        canvas.width = img.width;
        canvas.height = img.height;
        decoded = decoder.getDecoded();
    }

    test("Decoder without image should return 'No readable QRCode found'", () => {
        expect(decoded).toBe("No readable QRCode found");
    });
});

describe("Decoder with QR code", () => {
    const dom = new JSDOM(`<!DOCTYPE html>
    <html>
        <body>
            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAACXBIWXMAAA7EAAAOxAGVKw4bAAALcUlEQVRogd1abVBTVxp+BAM0TSA3hNFChwyYOOiM4ECBdMT+AeVDnGl3ZF2V0P6oVSmCTlcKdK1D3QVq6yry1XaccUuw2m7b7bgmAsKfBcdEC1PtTMNsKAxMBRxCghIiSVqzP9JzvDf3JkDLzs72mckM973nvOc855z7nveDVWazxYvfAEL+1xNYKawmfzQ3N2FoyLyszs3Nrbh37wfU19dR2YsvvoScnG0cfVKpFPX179I2PT3X8dVX/0BJycvIyMik8rKy0mWNn5S0AWVlh7lEhobMMJmMy1IEAE6nk9MvM1PD0yeXyzl9JicnYTIZUVCwgyP/JeMTrPYXXLr0GRiGCdrp0KEDGB0d4ciys3Nw7NibuHixA7m5OZibm6P6QkJWITc3h7ZdWFgAAJw61YDGxjNoa/sQiYnrAAAJCYloa/sw6Ph2ux179vw+OBGGYcAwDOx2O0+BRCJBeHg4QkNDee/CwsIQHR2Np56KAOA7TgAQErKKtvF4PJibm0NERATkcjkcDgfm5+fx008/0TahoaGIjo6Gy+WCw+HgjRNokXlECOO8vG08eW3tSd5x8Mf+/Qewf/8B+kx2oqurB0bjTRw+/Dp27SpCRcVRvP32n3DtmkFQT29vD06cOM6Td3ZeXzqRlYDZbMaDB7PYtCkZAGA03sTk5CQyMzUQiUQwGm8iKkqGzEwNxGLxrx7vv0akpaUJJpORrmBe3jZkZmrQ3NwKna4dhw+/jtrak3jjjT+uyHgrRmR0dAQ6XTt9npycAAB8/vnfOTKdrh2DgwMrNSzFihEZGhrC0NAQT37+/Ef07/HxcZw7d3alhuRAkIhEIkFt7UmePCUlhSdbs2YtamtP4s6db/Dll19QuVb7MlQqFaft8PAwdLqP6fMXX3wOo/EmSkvLsHbtWt5YQnOQSCSC1kyQSHh4+KLWiSAyMpK2ZRPJyMiARvM8p63ReJND5O7dO7h79w602hIekbi4ZxEX96zgmEsicujQAcF7go3x8TH699jYGKqqKpGSshmffHIZFy92QK+/ytL3Grxe4IMPnhyxHTsKsW9fMVpbW9Df38fTvWfP7qDjs++dgET8b+zF4Ha7MDxsgVqthlq9nndhjYzw9TEMA7V6Pb00ufrcGB62LGsOAMdpbF12ZyFUVBxFRcVRwXcazfO4fXsQOl070tNTee9v3x78xePydmRk5Hu6dWKxGHFxz8Jut8FqtWLNmrWIjIzE2NgY3G4XAGBqagoqlZqe8enpaczOPnFv4uOVvKPKMAxUKjXu35+iPhkAWCz/RlhYOJRKJR4+fIj796eCTp7MDwBgNlu87F98vNIrkUi9EonUm529zWs2W7w1Nce9EonUe/r0Wa/ZbPFu3pxK22zenMrpf/BgKX0nkUi9/f1Gr/8Y5FdUtNsrkUi9V67ovWazhaPv9OmzHD1CPzI/s9nipTty65YJVqsVWVkvwONxAwCiomQwGPRwu13Izy+A1ToNg0GPlJTNUKvVP6/K0zAY9HSVwsLCkZ9fQJ/7+/8FkSgMAKBQKDjxB0FfXx8sFt93MTf3EAaDHlbrNPLzCzA0NITR0RGkp2dAoVCgt7cHbrebvz2EUXb2Nt4KXrjQ7pVIpN6DB0sFV9BstnivXNFzVqmm5viiO+y/I0K/oqLdnB2+cKE9qD7Be2R+fh7nz38EkUiE8vIj2LhxI+f9xYsd1DoRdz8pKQnbt+fB6XSisfEMbet0OiEWi/Hqq6/B4/GgsfEMUlPTsHXrC7TN3r37oFDE4Ny5s1AoFNi7txhutwuNjWcgFotRXn4Eg4MDMJmM2LevGI8eLXA8BiDAhbiwsICODh118vzBvicIEhISodWWoLHxDDo6dJx3crkcWm0JdeMBcIgUFu6EWr0e586dhUzGQKstgcGgx4kTx1FefgRabQnKyko5TuiiRGpqqoS4cVBVVYO4uDiOTKGICdh+bm4OZWWlePBgliMvKXkFBQU70N7+Mex2O5qaWiAWPw0ASE/PQFNTCwYHB1BWVors7BwUF2tRX/8XjqULSGQpnmlycjLU6vWLtiPweDyC8bhKpYJKpUJj41kMD1s4ux8TE4OYmBiYTEaYTEYUF2uh0TyPEyeOw2azLU4EAGQyBpcvf4Zvv72L3Nwc7NpVhP37D6CysgoVFUdRVVWJ8fExdHX1BJx8XV0DUlPTOLLBwYGgO86O67Ozc1BZyW97+fJnmJmxBY7ZpVIp5HI57HY7QkJWITo6GhEREbDZbHA6nQB8nqdEIqEX3MzMDFUUHh4OiUQCtr7o6GjOYP4uicPhgMvlohewzWZDaGgooqKiApJlGDmAVZDL5Rx9lAjJO7FXJRCIA8h2M/LzC/DOO39etC8bp0418GL2hIREXLr0adB+DMPwTgMlEizGjo9XAvDFE1brNEeBWCzGpk3JiIqS0T6BQGJ0ok+lUtE8GAExItPT0/j++2GIRCJkZmoQFSUDAAwMfA2Px0P1bdiwAQCwiuR+/c0bO8YmEMp6qFRqXLr0KTWXBE1NLbx4ZDnwN78Eubk59GNnz4/uSHZ2DtRqNfT6f+LRI18CjcTYGzduRFrac9iyJYt37oOZXX9MTEygt7eH6rtxox8jIyMoLCz8+ewDdrsNV69epelWYkWzs3MQGxuLXbuK6DdLdpZD5KWXfsdjTGLs4mIt0tKeQ25uHnJz85Y8cX+Mj49x9HV1deLaNQM0Gg0lYrVaOXF9f38f+vv7oFarERsby8mZsUGJtLf/DcPDwygtfR0ulxvvvfeuYAcAaG1txtQU18VWKGI4MfaNGzdgMOhRWVnFsWbC+lqoBfK/7LZvz8WWLVlYt+5J/O9wOHDqVANUKhVKSl7hErl16xZMJiMNioIR6evr40Vx+fkFKC+voM8Gg57qW4yIf7jLRlLSBl7+wOVy4do1AzIzNZQI/djv3fsBTqcTCQmJALghr0zGICbmybfADqzGxsZQXf0mpFIp1qx5kkCYmLhH9ZF7x+l0YmLiHmQyBgqFggZW9fXvQql8ct7JYrW1tUChUEAmY1Bd/RaSk30W9ccff8To6AgnsKI74p+xCOaC+A8K+I6EkA8klAOYnbVzokilUskbj8QnVqsVVqsVTuc8fbd69Wpee0qEmN/lgB1jkwuReL/E/LKT2AQ6Xbtgoi49PZWacwK290s8ZwJB80vAPgqBMD4+RqO0sLBwTswupM8fgWJ2lUotuNuA76JUqdQYHR1ZWjqore1D3l3hjz17dtOPXalUBnUp2PksgsLCnSgs3Mm7YIPpqa5+CwD3emBD0Pt1uVzo7eV7tikpKbxv6eHDhxyr45/H6unxeQo5Ofx6CwE7Zie4c+cbAL4SHjsnkJW1FfPz87z5CRJxOByCRZba2pM8IvfvTwm2JSBmPBiRtraWgO+6u7vQ3d1Fn4kLtSQiy4FCoUB5+RGenO0++GNg4Gv09/dBqVQK9gV8O9Hd3YWsrK2cuIYUhcrLj+CZZ56h8l9NhGHkHKduKfjuu+/Q0aELWsozGPTo7u5CamqaoH5/2a8mEqjOTlBX10BjdlIXJw4qcWMI2HV7/3CB1O3r6hoA+HILgnX2X4pAdXaCtLTnMDMzg2PH3qCy2NhYxMbGUjcmmD4CUrcnsYh/G0EiDMMIVk8X85nYqK5+k7rgUVEydHZepzkAAqE6R0JCImds8k3U1TXA4/Hg0CGf99vZeR0ikSgwEaH6Ontgh8PBq4uz/7PB632MmZkZunJseDwe2Gw2RERELLuSGxkZCQB48GAWjx8/5r3nEfHPTiyGxMR1HPejsfEM8vK2oampBe+//1cAviQFu24frM4+OjqCvXv/QJ/9I0QAmJ2d5UWwlEhS0oZlEQiE+HglJ8YGQONudhtAOGYXi8UcGdvEAkBqahp1a9hzpm78/zt+M/+v9R8GssKWi/LRXAAAAABJRU5ErkJggg==" />
            <canvas />
        </body>
    </html>`, { resources: "usable" });

    const canvas = dom.window.document.body.getElementsByTagName("canvas")[0];
    const img = dom.window.document.body.getElementsByTagName("img")[0];

    const decoder: Decoder = new Decoder(img, canvas);

    expectTypeOf(decoder.getDecoded).toBeFunction()

    let decoded: string = "";

    img.onload = () => {
        canvas.width = img.width;
        canvas.height = img.height;
        decoded = decoder.getDecoded();
    }

    test("Decoder without image should return 'QR 2 Text is great'", () => {
        expect(decoded).toBe("QR 2 Text is great");
    });
});