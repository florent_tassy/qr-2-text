import "@testing-library/jest-dom";
import { render, fireEvent, screen } from '@testing-library/svelte'
import ResultArea from "../lib/ResultArea.svelte";
import { vi } from "vitest";

const decodedText = "Decoded text";
const decodedTextProp = { props: { decoded: decodedText } };

describe("ResultArea", () => {
    test("shows the textarea when rendered", () => {
        render(ResultArea, decodedTextProp);
        expect(screen.getByRole("textbox")).toBeInTheDocument();
    });

    test("shows the decoded text in result area", () => {
        render(ResultArea, decodedTextProp);
        expect(screen.getByRole("textbox")).toBeInstanceOf(HTMLTextAreaElement);
        expect(screen.getByRole("textbox")).toHaveValue(decodedText);
    });

    test("shows the copy button when rendered", () => {
        render(ResultArea, decodedTextProp);
        expect(screen.getByRole("button")).toBeInTheDocument();
    });

    test("must copy the textarea value when button is clicked", async () => {
        let originalClipboard: Clipboard;

        beforeAll(() => originalClipboard = navigator.clipboard);

        afterAll(() => Object.assign(navigator, originalClipboard));

        let mockClipboardText: string = "";

        Object.assign(navigator, {
            clipboard: {
                writeText: (text: string) => { mockClipboardText = text; },
                readText: () => { return mockClipboardText; }
            },
        });

        vi.spyOn(navigator.clipboard, "writeText");

        render(ResultArea, decodedTextProp);
        const button = screen.getByRole("button");

        fireEvent.click(button);

        expect(navigator.clipboard.writeText).toHaveBeenCalled();
        expect(navigator.clipboard.readText()).toEqual(decodedText);

        Object.assign(navigator, originalClipboard);
    });
});