import jsQR, { type QRCode } from "jsqr";

export default class Decoder {
    private canvas: HTMLCanvasElement;
    private context: CanvasRenderingContext2D;
    private img: HTMLImageElement;
    private readonly defaultMessage: string = "No readable QRCode found";

    public constructor(img: HTMLImageElement, canvas: HTMLCanvasElement) {
        this.img = img;
        this.canvas = canvas;
        this.context = this.canvas.getContext("2d");
    }

    /* https://stackoverflow.com/questions/32272904/converting-blob-file-data-to-imagedata-in-javascript */
    public getDecoded(): string {
        this.context.fillStyle = "#ffffff";
        this.context.fillRect(0, 0, 500, 500);
        this.context.fill();
        this.context.drawImage(this.img, 0, 0);

        let decoded: string = this.defaultMessage;

        const code: QRCode = jsQR(this.context.getImageData(0, 0, this.canvas.width, this.canvas.height).data, this.img.width, this.img.height);

        if (code && code.data) {
            decoded = code.data;
        }

        return decoded;
    }
}